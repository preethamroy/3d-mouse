Project Proposal for EL6483
==========================

## Team




1. Abhishek Pillai - ap4395@nyu.edu

2. Dhishan Amaranath - da1683@nyu.edu

3. Karthik Hosakere Suresh - khs308@nyu.edu

4. Preetham Roy Pothakamuri - prp295@nyu.edu

## Idea

#### 3D Mouse, a tri-axial gesture controlled PC mouse
The pointing device has been used in GUI based operating systems since its inception, and throughout its history it has advanced through many different types of technologies. 3D Mouse will provide a new dimension to the historically used desktop mice.

The device is able to implement all the functions of the traditional mouse, by exclusively using simple hand gestures. No line of sight required. No flat surface required. The relative orientation from the two sensors mounted on the index finger and the thumb of the gloves, control the movement of the mouse and other functionality like clicks and scroll. All buttons and scroll will be replaced by the gestures.

![](http://i.imgur.com/NetSUWK.jpg)

## Materials

**STM32F4 Discovery board**, primary MCU

[**GY-521 MPU6050 Module**](http://www.dx.com/p/gy-521-mpu6050-3-axis-acceleration-gyroscope-6dof-module-blue-154602#.VRXCVHXd_ew), this is a 3-axis accelerometer and gyroscope module which will be attached to the finger. It will be used to read the orientation of the fingers which will control the mouse movement. The datasheet for the MPU6050 chip is [here](http://www.invensense.com/mems/gyro/mpu6050.html).

## Milestones

* ***16 April 2015***: Develop the code for recognizing multiple hand gestures, use LEDS to simulate mouse functions

* ***7 May 2015***: Complete functionality of traditional mouse. Attempt wireless connectivity and complex hand gestures. Report and presentation.

## Plan of work
Division of work

* Karthik Suresh – Programming the Discovery board to work as a computer mouse,
* Dhishan Amaranath – Configuring I2C between the input and the board, testing and integration
* Abhishek Pillai – Mapping raw input data to programmable code for the board
* Preetham Roy – Taking reading from input devices, testing the code

Week 1           | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Code structure with flow control. Write step function and comments | 4
Dhishan Amaranath | Pinout diagrams and I2C schematics for the modules and STM board | 5
Karthik Suresh | Code the initial makefile, include all necessary header files and libraries |4
Preetham Roy | Finding existing codes and implementation which can be reused | 4
Team | Initialize all code so as to be ready for debugging next week | 17

Week 2           | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Module Soldering, Basic understanding of Accelerometer raw data, Gyroscope data and theory on using both the data to provide an accurate tilt and accelerations in 3 directions | 5
Dhishan Amaranath | Establishing I2C communication between module and STM for one module | 4
Karthik Suresh | Functions for reading raw data from the modules for all three accelerometer axes and from the gyroscope | 5
Preetham Roy | Code re-structure based on state machine for basic 4 direction detection | 5
Team | Bare minimum of each task individually functional for integration next week | 19


Week 3       | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Functions for calculating the pitch, roll, and Yaw using both data from accelerometer and the gyroscope | 5
Dhishan Amaranath | Algorithm which triggers left, right, up & down in the state machine code | 5
Karthik Suresh | Initial GPIO configuration for LED's and then from the data provided by the other team members light the appropriate LED's | 5
Preetham Roy | Initialization communication between PC and the Board to use as HID mouse | 5
Team | Testing the full code flow for 4 hand gestures and display the output through the LED's for first Milestone, documentation | 20



Week 4       | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Second sensor calibrating for mouse click and benchmarking values using the debugger,Testing of the library with hard coded values and display of mouse moment with STM32 board | 5
Dhishan Amaranath | Software interrupts to interrupt clicks, I2C for the second sensor | 5
Karthik Suresh | Functions to read values from the second module based on the pinout earlier made, Functions for mouse clicks and drag using hardcoded values | 6
Preetham Roy | Algorithm which calculates the increment or decrement of the x,y co-ordinates based on the pitch, roll which will be later used for controlling the mouse pointer | 5
Team | Functional blocks for second sensor individually working | 21

Week 5       | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Final integration of both the sensors and calibration with the finger movement to simulate mouse using debugger | 6
Dhishan Amaranath | Integration of library and the sensor data for the mouse movement in 4 directions and the diagonal | 6
Karthik Suresh | Experimenting the mouse moment with the one sensor values and buttons on the board | 6
Preetham Roy | Functions for Drag, Click, Double Click using the second sensor benchmarked values | 6
Team | Final integration of two sensors and gesture controlled mouse | 24

Week 6       | Tasks     | Hours
----------------|-----------|---
Abhishek Pillai | Beta testing code for bugs, documentation | 5
Dhishan Amaranath | Code cleanup and optimization, documentation | 5
Karthik Suresh | Beta testing code for bugs, documentation | 5
Preetham Roy | Code cleanup and optimization, documentation | 5
