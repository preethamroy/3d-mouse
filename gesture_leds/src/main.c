#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"


const uint16_t LEDS = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
//const uint16_t USER_BUTTON = GPIO_Pin_0;
const uint16_t npins = 4;
const uint16_t pin[4] = {12,13,14,15};


void init();
void initLeds();


int main() {
    init();
    while (1); 
}


void initLeds() {
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;// Enable GPIOD Clock
    unsigned int i=npins;

    while(i>0) {
	GPIOD->MODER |= (1 << 2*pin[i-1]);	// Set GPIOD pin 12-15
	GPIOD->MODER &= ~(1 << (2*pin[i-1]+1));	// mode to output
	i--;
    }
}
